//
//  main.cpp
//  pub-sdbus
//
//  Created by dev on 2022-02-22.
//  Copyright © 2022 Root Interface. All rights reserved.
//

#include <systemd/sd-bus.h>
#include <signal.h>
#include <string>
#include <thread>
#include <chrono>
#include <stdexcept>
#include <assert.h>

#include "IPSME_MsgEnv.h"

bool handler_str_(sd_bus_message* p_msg, std::string str_msg)
{
	printf("%s: [%s] \n", __func__, str_msg.c_str());


	return true;
}

std::string bus_message_str(sd_bus_message* p_msg) {
	const char* psz= nullptr;
	int i_r = sd_bus_message_read_basic(p_msg, SD_BUS_TYPE_STRING, &psz);
	if (i_r < 0)
		throw i_r;

	// optionally, move the "read pointer" in the message to the beginning of the message
	// sd_bus_message_rewind(p_msg, true);

	return psz;
}

void handler_(sd_bus_message* p_msg)
{
	// printf("%s: \n", __func__);

	try {
		char ch_type; 
		if (sd_bus_message_peek_type(p_msg, &ch_type, nullptr) && (ch_type=='s') && handler_str_(p_msg, bus_message_str(p_msg) ))
			return;

	}
	catch (int &i_r) {
		printf("ERR: error is message execution: %s \n", strerror(-i_r));
	}
	catch (...) {
		printf("ERR: error is message execution \n");
		return;
	}

	printf("%s: DROP! \n", __func__);
}

//----------------------------------------------------------------------------------------------------------------

bool gb_quit_= false;

void handler_sigint_(int s)
{
	printf("\nCaught SIG[%d]\n", s);

	// if the user presses ^C twice, then just exit.
	if (gb_quit_)
		exit(s);

	gb_quit_= true;
}

int main(int argc, const char * argv[])
{
	// -----

	// https://stackoverflow.com/questions/1641182/how-can-i-catch-a-ctrl-c-event
	struct sigaction sa;

	sa.sa_handler = handler_sigint_;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;

	sigaction(SIGINT, &sa, NULL);

	// -----

	if (argc < 3)
	{
		printf("USAGE: %s <0|1> <string>\n\n", argv[0]);
		return -1;
	}
	
	IPSME_MsgEnv::subscribe(&handler_);

	gb_quit_= (argv[1][0] == 0x31) ? false : true;

	printf("main(): pub -> sdbus -- [%s]\n", argv[2]);
		
	int i_r= IPSME_MsgEnv::publish("s", argv[2]);
	assert(i_r);

	printf("%s: Listening ... %s \n", __func__, (gb_quit_ ? "NOT." : "") );
	fflush(stdout);

	do {
		IPSME_MsgEnv::process_requests();

        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
    while (! gb_quit_);

	return 0;
}

